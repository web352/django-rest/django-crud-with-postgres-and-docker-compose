# Что это?
Простое CRUD (в реальности только CR) приложение. Директория с кодом монтируется в докер контейнер и редактируя код локально в IDE, dev сервер подхватит изменения.
# Сборка
Расчитана на запуск с помощью compose:
```bash
docker-compose up
```
---
При запросе на корневой урл получаем 500-ую.
```bash
$ curl -I http://0.0.0.0:8000/
HTTP/1.1 500 Internal Server Error
Date: Thu, 28 Oct 2021 22:11:15 GMT
Server: WSGIServer/0.2 CPython/3.9.5
Content-Type: text/html
X-Frame-Options: DENY
Content-Length: 138567
Vary: Cookie
X-Content-Type-Options: nosniff
Referrer-Policy: same-origin
```
И в действительности - это нормальное поведение. Каким-то образом нужно выполнить миграцию схемы БД и делать это изнутри контейнера с приложением кажется неправильным.
```bash
db_init.sh
```
Выполнив скрипт - далем миграцию и загружаем фикстуры, получаем 200.
