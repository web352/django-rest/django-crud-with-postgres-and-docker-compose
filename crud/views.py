from django.shortcuts import render
from django.views.generic import CreateView, ListView
from .models import Post

class Posts(ListView):
    model = Post


class CreatePost(CreateView):
    model = Post
    success_url = "/"
    fields = "__all__"
