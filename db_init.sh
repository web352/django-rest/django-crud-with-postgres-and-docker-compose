#!/bin/bash
docker exec -it $(docker ps --filter 'name=web' --format '{{.ID}}') python3 manage.py migrate
docker exec -it $(docker ps --filter 'name=web' --format '{{.ID}}') python3 manage.py flush --noinput
docker exec -it $(docker ps --filter 'name=web' --format '{{.ID}}') python3 manage.py loaddata crud/fixtures/post.json

echo "Fixtures has been loaded!"

curl -i -X GET http://0.0.0.0:8000/
